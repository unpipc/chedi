import operator
from chedi import *
from datetime import datetime

print lexer.__file__

blank = Lexem('blank', r'\s+', ignore=True)
num = Lexem('num', r'[-+]?(\d+\.?\d*)|(\.\d+)')
plus = Lexem('plus', r'\+')
minus = Lexem('minus', r'-')
mult = Lexem('mult', r'\*')
div = Lexem('div', r'/')
ob = Lexem('open_brace', r'\(')
cb = Lexem('close_brace', r'\)')

sketch = Sketch()

sketch.define('S', [nt('T'), nt('R')], target=True)

sketch.define('R', {(plus, nt('T'), nt('R')),
                    (minus, nt('T'), nt('R'))}, or_lambda=True)

sketch.define('T', (nt('E'), nt('F')))

sketch.define('F', {(mult, nt('E'), nt('F')),
                    (div, nt('E'), nt('F'))}, or_lambda=True)

sketch.define('E', {(ob, nt('S'), cb),
                    num})


def S(s):
    t, r = s[:]

    return R(T(t), r)


def R(left_operand, r):
    if r.node_type == 'lambda':
        return left_operand
    else:
        ops = {'plus': operator.add, 'minus': operator.sub}
        op = ops[r[0].token]
        t, r = r[1:]

        return R(op(left_operand, T(t)), r)


def T(t):
    e, f = t[:]

    return F(E(e), f)


def F(left_operand, f):
    if f.node_type == 'lambda':
        return left_operand
    else:
        ops = {'mult': operator.mul, 'div': operator.div}
        op = ops[f[0].token]
        e, f = f[1:]

        return F(op(left_operand, E(e)), f)


def E(e):
    if e[0].token == 'open_brace':
        return S(e[1])
    else:
        return float(e[0].text)


def time(fun, text):
    t0 = datetime.now()
    r = fun()
    print '{}: {}'.format(text, datetime.now() - t0)
    return r


lex = Lexer()
lex.add_lexems([blank, num, plus, minus, mult, div, ob, cb])

text = '(1.2 + (2.1 * 3 - (8.34 / 4 - .3)) / 2) * 2'
bit = '(1 + 3 - (4.0/5 + 64.3))'
text = ' + '.join([bit] * 100)


print 'input text: "{}"'.format(text)

tokens = time(lambda: list(lex.analyze(text)), 'tokenization')
ll1 = time(lambda: LL1(sketch), 'creating parser')
tree = time(lambda: ll1.parse(tokens), 'syntax analysis')


print 'result: {}'.format(time(lambda: S(tree), 'semantics analysis'))
print 'python result: {}'.format(time(lambda: eval(text), 'python\'s eval()'))
