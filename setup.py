from setuptools import setup

DESCRIPTION = 'LL(1) parser and lexer'

setup(name='chedi',
      version='0.1',
      description=DESCRIPTION,
      long_description=DESCRIPTION,
      keywords='ll1 parser lexer',
      url='https://unpipc@bitbucket.org/unpipc/chedi',
      author='Sergey Kozlov',
      author_email='skozlov@ptsecurity.com',
      packages=['chedi'],
      zip_safe=True)