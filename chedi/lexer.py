import re
import unittest
from collections import OrderedDict


class LexBase(object):
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name


class Token(LexBase):
    def __init__(self, name, chunk, span):
        super(Token, self).__init__(name)

        self.chunk = chunk
        self.span = span

    def __repr__(self):
        return '{:8s} {:3d} {:3d} {}'.format(self.name, self.span[0], self.span[1], self.chunk)

eof_token_name = '.eof'


def eof_token(at):
    return Token(eof_token_name, '', (at, at))


class Lexem(LexBase):
    def __init__(self, name, pattern=None, **params):
        self.pattern = name if pattern is None else pattern
        self.params = params

        pattern = r'[a-zA-Z_][a-zA-Z_0-9]*'

        # `name' is used as re group names, so cannot be any string.
        if not re.match(pattern, name):
            raise ValueError('Wrong lexem name. It must match pattern {}'.format(re.escape(pattern)))

        if re.match(self.pattern, ''):
            raise ValueError('Regular expression matches empty string')

        super(Lexem, self).__init__(name)

    def get_group_pattern(self):
        return '(?P<{}>{})'.format(self.name, self.pattern)

    def __repr__(self):
        return '{}:{}'.format(self.name, self.pattern)


class MatchError(Exception):
    def __init__(self, index):
        super(MatchError, self).__init__('Cannot match any lexem at index {:d}'.format(index))


class Lexer(object):
    def __init__(self):
        self.lexems = OrderedDict()

    def add_lexem(self, lexem):
        if lexem.name in self.lexems:
            raise ValueError("Lexem `{}' already exists".format(lexem.name))

        self.lexems[lexem.name] = lexem

    def add_lexems(self, lexems):
        map(self.add_lexem, lexems)

    def get_pattern(self):
        return '|'.join(map(lambda l: l.get_group_pattern(),
                            self.lexems.values()))

    def match(self, text):
        m = re.match(self.get_pattern(), text)

        if m:
            name, chunk = [(k, v) for k, v in m.groupdict().items() if v is not None][0]

            return name, chunk

    def analyze(self, text):
        pattern = re.compile(self.get_pattern())
        cursor = 0

        while cursor < len(text):
            m = pattern.match(text[cursor:])

            if m:
                start, end = m.span()
                start += cursor
                end += cursor
                cursor = end

                name, chunk = [(k, v) for k, v in m.groupdict().items() if v is not None][0]

                if not self.lexems[name].params.get('ignore', False):
                    yield Token(name, chunk, (start, end))
            else:
                raise MatchError(cursor)


def tokens(iterable):
    at = 0

    for item in iterable:
        if not isinstance(item, basestring):
            raise ValueError('not a string: {}'.format(item))

        yield Token(item, item, (at, at+len(item)))
        at += len(item)


class Test(unittest.TestCase):
    def test_lexem(self):
        Lexem('keyword', r'function')
        self.assertRaises(ValueError, Lexem, 'string', r'')
        self.assertRaises(ValueError, Lexem, 'string', r'\s*')
        self.assertRaises(ValueError, Lexem, '0_bad_id', r'term')
        self.assertRaises(ValueError, Lexem, '._bad_id', r'term')

    def test_identical(self):
        l1 = Lexem('keyword', r'function')
        l2 = Lexem('keyword', r'operator')

        lexer = Lexer()
        lexer.add_lexem(l1)
        self.assertRaises(ValueError, lexer.add_lexem, l2)

    def test_analyze(self):
        b = Lexem('blank', r'\s+', ignore=True)
        var = Lexem('var', r'[a-zA-Z_][a-zA-Z_0-9]*')
        eq = Lexem('eq', r'=')
        delim = Lexem('delim', r';')
        _str = Lexem('str', r'"([^"\\]|(\\.))*"')
        _int = Lexem('int', r'[1-9][0-9]*')

        l = Lexer()
        l.add_lexems((b, var, eq, delim, _str, _int))

        tokens = list(l.analyze('a = "function description"; b = 146'))
        expected = ['var', 'eq', 'str', 'delim', 'var', 'eq', 'int']
        self.assertEqual(map(lambda t: t.name, tokens), expected)

    def test_lexems_and_tokens_identity(self):
        var = Lexem('var', r'[a-zA-Z_][a-zA-Z_0-9]*')
        l = Lexer()
        l.add_lexem(var)
        token = list(l.analyze('a'))[0]
        self.assertEqual(token.name, var.name)
        # self.assertEqual(len({token, var}), 1)
        # self.assertEqual(len({token: 0, var: 1}), 1)
