import json
from collections import namedtuple

TTERM = 0
THEAD = 1
TLAMBDA = 2

RawNode = namedtuple('RawNode', ['sym', 'type', 'n_children', 'children'])


class Node(object):
    node_type = 'abstract'

    def as_dict(self):
        pass

    def as_json(self, indent=2):
        return json.dumps(self.as_dict(), indent=indent, sort_keys=True)


class Lambda(Node):
    node_type = 'lambda'

    def __init__(self, sym):
        self.sym = sym

    def as_dict(self):
        return dict(type=self.node_type,
                    sym=self.sym)


class NT(Node):
    node_type = 'nt'

    def __init__(self, sym, children=None):
        self.sym = sym
        self.children = list(children) if children else []

    def add_child(self, child):
        self.children.append(child)

    def __getitem__(self, item):
        return self.children[item]

    def as_dict(self):
        return dict(type=self.node_type,
                    sym=self.sym,
                    children=map(lambda ch: ch.as_dict(),
                                 self.children))


class T(Node):
    node_type = 't'

    def __init__(self, token):
        self.token = token.name
        self.text = token.chunk
        self.starts = token.span[0]
        self.ends = token.span[1]

    def as_dict(self):
        return dict(type=self.node_type,
                    token=self.token,
                    text=self.text,
                    starts=self.starts,
                    ends=self.ends)