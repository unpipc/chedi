import re

from collections import defaultdict
from definition import Def, NT, nt
from lexer import Lexem
from errors import claim
from common import *

ID_PATTERN = r'[a-zA-Z_][a-zA-Z_0-9]{0,63}'
LIST_TYPES = (list, tuple)
SET_TYPES = (set, frozenset)


class Sketch(object):
    def __init__(self):
        self.records = defaultdict(set)
        self.target_sym = None
        self.lambdas = set()

    def define(self, sym, record, target=False, or_lambda=False):
        """
        :param sym:      Name of non-terminal symbol to be defined.
        :param record:   Definition record, e.g.
                           o chain of terminal or non-terminal symbols.
                           o set of chains
                         Chains can be recursive, i.e. contain other records.
        :return:         None
        """

        claim(sym not in self.records, 'Symbol {} already defined'.format(sym))
        claim(re.match(ID_PATTERN, sym), 'Wrong symbol name. It must match pattern {}'.format(ID_PATTERN))

        self.records[sym] = record

        if or_lambda:
            self.lambdas.add(sym)

        if target:
            claim(self.target_sym in {None, sym}, 'Target already specified')
            self.target_sym = sym

        return nt(sym)

    def get_defs(self):
        def_lists = [self.record_to_defs(*item) for item in self.records.iteritems()]
        return {d.sym: d for d in reduce(lambda l1, l2: l1 + l2, def_lists, [])}

    def record_to_defs(self, sym, record):
        '''
            Creates non-terminal definitions out of records.
            Ideally, a record is set of tuples, i.e. set of chains.
            Set of tuples is an input for the Def class constructor.
            If it is not set of tuples, we cast it recursively
            creating side definitions.

        :param sym: str|unicode Name of symbol being defined
        :param record: See docstring above.
        :return: Yields definitions (Def() instances)
        '''
        claim(isinstance(record, LIST_TYPES + SET_TYPES),
                  'Record must be list, tuple, set or frozenset')
        claim(not isinstance(record, Def),
                  'Record is already a definition: {}'.format(record))

        sym2 = new_sym(sym, new_level=True)
        chains = []
        new_defs = []

        if type(record) in LIST_TYPES + SET_TYPES:
            claim(len(record), "Empty definition.")

        if type(record) in LIST_TYPES:
            chain, new_defs = self._to_chain(sym2, record)
            chains = [chain]
        elif type(record) in SET_TYPES:
            for item in record:
                if new_defs:
                    sym2 = new_sym(new_defs[-1].sym)

                if type(item) in LIST_TYPES:
                    chain, new_defs = self._to_chain(sym2, item)
                elif type(item) in SET_TYPES:
                    new_defs = self.record_to_defs(sym2, item)
                    chain = (nt(sym2),)
                else:
                    chain, new_defs = self._to_chain(sym2, (item,))

                chains.append(chain)

        new_defs.append(Def(sym, chains, can_be_lambda=sym in self.lambdas))

        return new_defs

    def _to_chain(self, sym, record):
        assert type(record) in LIST_TYPES

        chain = []
        new_defs = []

        for item in record:
            if type(item) in LIST_TYPES + SET_TYPES:
                new_defs.extend(self.record_to_defs(sym, item))
                item = nt(sym)
                sym = new_sym(sym)

            if isinstance(item, Lexem):
                item = item.name

            claim(isinstance(item, (NT, basestring)), 'Sketch item must be a string, Lexem object, or NT object')
            chain.append(item)

        return tuple(chain), new_defs

