#!/usr/bin/env python

import itertools
import unittest

import nodes

from common import *
from errors import *
from operator import itemgetter
from collections import defaultdict
from sketch import Sketch
from definition import Def, nt, is_nt
from lexer import Token, eof_token_name, eof_token, tokens


def find_repeats(iterable):
    repeats = []
    included = False

    for i, x in enumerate(sorted(iterable)):
        if i:
            if x == prev:
                if not included:
                    repeats.append(x)
                    included = True
            else:
                included = False

        prev = x

    return repeats


class LL1(dict):
    def __init__(self, sketch, options=''):
        defs = sketch.get_defs()
        claim(defs, 'Empty sketch.')

        super(LL1, self).__init__(defs)

        self.target_sym = self.keys()[0] if len(self) == 1 else sketch.target_sym

        claim(self.target_sym, 'Target symbol undefined.')
        claim(self.target_sym in defs, 'No definition for target {}'.format(self.target_sym))

        allowed_opts = set('fr')
        invalid_opts = set(options) - allowed_opts

        claim(not invalid_opts,
              'Unknown options: {}. Allowed options are {}'.format(one_string(invalid_opts),
                                                                   one_string(allowed_opts)))

        self._recalculate_lambdas()

        non_prod = set(self.keys()) - set(self.lambda_syms)

        goon = True
        while goon:
            goon = False

            for sym in set(non_prod):
                d = self[sym]

                for chain in d.chains:
                    nt_refs = set(map(lambda nt: nt.sym, filter(is_nt, chain)))

                    if not (nt_refs & non_prod):
                        goon = True
                        non_prod.discard(d.sym)

        claim(not non_prod, 'Non-productive rules: {}'.format(', '.join(sorted(non_prod))))

        for opt in options:
            if opt == 'f':
                self.left_factor()

            if opt == 'r':
                self.eliminate_left_recursion()

        self.add_initials()
        self.add_followers()

    def __get_lambda_syms(self):
        return map(lambda d: d.sym, filter(lambda d: d.can_be_lambda, self.itervalues()))

    lambda_syms = property(__get_lambda_syms)

    def __str__(self):
        defs = self.values()
        width = max(map(lambda d: len(d.sym), defs))

        return '\n'.join(map(lambda d: d.to_str(width),
                         sorted(defs, key=lambda d: d.sym)))

    def _recalculate_lambdas(self):
        goon = True
        while goon:
            goon = False

            for d in filter(lambda d: not d.can_be_lambda, self.itervalues()):
                for c in d.chains:
                    if not list(itertools.dropwhile(lambda x: is_nt(x) and self[x.sym].can_be_lambda, c)):
                        d.can_be_lambda = True
                        goon = True
                        break

    def left_factor(self):
        new_defs = []
        more_defs = self.values()

        while more_defs:
            defs = more_defs
            more_defs = []

            for d in defs:
                sym = d.sym
                new_level = True
                new_chains = []

                for start in set(map(itemgetter(0), d.chains)):
                    chains = filter(lambda c: c[0] == start, d.chains)

                    if len(chains) == 1:
                        new_chains.append(chains[0])
                    else:
                        tail_def = Def(new_sym(sym, ':', new_level=new_level),
                                       map(lambda c: c[1:], chains))
                        new_chains.append((start, nt(tail_def.sym)))
                        sym = tail_def.sym
                        new_level = False

                        # It may need factoring too.
                        more_defs.append(tail_def)

                new_defs.append(Def(d.sym, new_chains, can_be_lambda=d.can_be_lambda))
                # Think of recalculating lambdas.

        self.clear()
        self.update({d.sym: d for d in new_defs})

    def eliminate_left_recursion(self):
        xlr_defs = set()

        for d in self.itervalues():
            main_d, rec_d = self.__x_lr(d)

            for xlr_d in xlr_defs:
                main_d = self.__lsub(main_d, xlr_d)

            xlr_defs.add(main_d)
            if rec_d: xlr_defs.add(rec_d)

        self.clear()
        self.update({d.sym: d for d in xlr_defs})

    def __x_lr(self, d):
        lr = lambda c: is_nt(c[0]) and c[0].sym == d.sym

        tails = map(lambda c: c[1:], filter(lr, d.chains))
        non_lr_chains = list(filter(lambda c: not lr(c), d.chains))

        rec_sym = "{}'".format(d.sym)

        if tails:
            rec_def = Def(rec_sym,
                          map(lambda t: t + (nt(rec_sym),), tails),
                          can_be_lambda=True)
            main_def = Def(d.sym,
                           map(lambda c: c + (nt(rec_sym),), non_lr_chains))

            return main_def, rec_def
        else:
            return d, None

    def __lsub(self, d, d_sub):
        def sub_cond(x):
            return is_nt(x) and x.sym == d_sub.sym

        sub = False
        chains = d.chains

        goon = True
        while goon:
            goon = False
            new_chains = []

            for c in chains:
                if sub_cond(c[0]):
                    new_chains.extend(map(lambda parts: parts[0] + parts[1],
                                          itertools.product(d_sub.chains, [c[1:]])))
                    if d_sub.can_be_lambda and c[1:]:
                        new_chains.append(c[1:])

                    goon = True
                    sub = True
                else:
                    new_chains.append(c)

            chains = new_chains

        if sub:
            return Def(d.sym, chains, can_be_lambda=d.can_be_lambda)
        else:
            return d

    def add_initials(self):
        initials = defaultdict(lambda: defaultdict(set))

        for sym, d in self.iteritems():
            for i, c in enumerate(d.chains):
                for x in c:
                    if is_nt(x):
                        claim(x.sym in self, 'Unknown definition {}'.format(x.sym))
                        initials[sym]['nt'].add((x.sym, i))

                        if self[x.sym].can_be_lambda:
                            continue
                    else:
                        initials[sym]['t'].add((x, i))

                    break

        modified = True
        while modified:
            modified = False

            for sym, data in initials.iteritems():
                num_t = len(data['t'])

                for sym, chain_no in data['nt']:
                    data['t'] |= set(zip(map(itemgetter(0), initials[sym]['t']),
                                         itertools.repeat(chain_no)))

                modified = modified or num_t != len(data['t'])

        for sym, data in initials.iteritems():
            self[sym].initials = dict(data['t'])
            ambiguous_t = find_repeats(map(itemgetter(0), data['t']))

            claim(not ambiguous_t,
                  'LL(1): non-terminal {} has chains '
                  'starting with the same terminal {}.'.format(sym, one_string(ambiguous_t)))

    def add_followers(self):
        followers = defaultdict(lambda: defaultdict(set))
        followers[self.target_sym]['t'] = {eof_token_name}

        for sym, d in self.iteritems():
            followers[sym] = followers[sym]

            for c in d.chains:
                prev_syms = []

                for x in c:
                    if is_nt(x):
                        try:
                            inits = set(self[x.sym].initials.keys())
                        except KeyError as e:
                            raise LLError('No rule for {}'.format(e))

                        for p in prev_syms:
                            followers[p]['t'] |= inits

                        if self[x.sym].can_be_lambda:
                            prev_syms.append(x.sym)
                        else:
                            prev_syms = [x.sym]
                    else:
                        for p in prev_syms:
                            followers[p]['t'].add(x)

                        prev_syms = []

                for p in prev_syms:
                    followers[p]['take_from'].add(sym)

        modified = True
        while modified:
            modified = False

            for sym, data in followers.iteritems():
                num_t = len(data['t'])

                for take_from in data['take_from']:
                    data['t'] |= followers[take_from]['t']

                modified = modified or num_t != len(data['t'])

        for sym, data in followers.iteritems():
            self[sym].followers = data['t']

    def check_left_recursion(self):
        refs = defaultdict(lambda: defaultdict(set))

        for d in self.itervalues():
            refs[d.sym]['to'] = set(map(lambda x: x.sym,
                                        filter(is_nt, map(itemgetter(0), d.chains))))

            for sym in refs[d.sym]['to']:
                refs[sym]['by'].add(d.sym)

        goon = True
        while goon:
            goon = False

            for ref in refs.itervalues():
                for f in ref['by']:
                    num_refs = len(refs[f]['to'])
                    refs[f]['to'] |= ref['to']

                    if num_refs != len(refs[f]['to']):
                        goon = True

        self_refs = filter(lambda (sym, ref): ref['by'] & ref['to'],
                           refs.iteritems())

        claim(not self_refs, 'There are left-recursive rules: {}'.format(', '.join(map(itemgetter(0),
                                                                                       self_refs))))

    def parse(self, input_seq):
        self.check_left_recursion()

        expected = [nt(self.target_sym)]
        input_iter = iter(input_seq)
        cursor = -1
        t_read = False
        production = []
        tokens = []

        while expected:
            if not t_read:
                try:
                    token = next(input_iter)
                except StopIteration:
                    token = eof_token(cursor+1)

                claim(isinstance(token, Token),
                      'Expected object of class Token, not {}'.format(type(token).__name__))

                t_read = True
                cursor += 1

            top = expected.pop()

            if is_nt(top):
                d = self[top.sym]
                chain_no = d.initials.get(token.name)

                if chain_no is None:
                    if token.name in d.followers and self[top.sym].can_be_lambda:
                        pass
                    else:
                        syntax_error(cursor, token.name == eof_token_name)
                else:
                    new_chain = d.chains[chain_no]
                    expected.extend(reversed(new_chain))

                production.append(chain_no)
            elif top == token.name:
                tokens.append(token)
                t_read = False
            else:
                syntax_error(cursor, token.name == eof_token_name)

        try:
            next(input_iter)
            expected_eof(cursor + 1)
        except StopIteration:
            pass

        return self.__build_production_tree(production, tokens)

    def __build_production_tree(self, production, tokens):
        class Frame(object):
            def __init__(self, definition, chain_no):
                self.definition = definition
                self.chain_no = chain_no

                node_cls = nodes.Lambda if self.chain_no is None else nodes.NT
                self.node = node_cls(self.definition.sym)

                self.symbols = self.__symbols()

            def __symbols(self):
                if self.chain_no is not None:
                    for x in self.definition.chains[self.chain_no]:
                        yield x

        frames = [Frame(self[self.target_sym], production.pop(0))]
        top_node = frames[0].node

        while frames:
            frame = frames[-1]
            switch_frame = False

            for x in frame.symbols:
                if is_nt(x):
                    new_frame = Frame(self[x.sym], production.pop(0))
                    child = new_frame.node
                    switch_frame = True
                    frames.append(new_frame)
                else:
                    child = nodes.T(tokens.pop(0))

                frame.node.add_child(child)

                if switch_frame:
                    break

            if not switch_frame:
                frames.pop()

        return top_node


class LL1Test(unittest.TestCase):
    def test_basic_sketch(self):
        s = Sketch()
        self.assertRaises(LLError, s.define, '.A', 1)

        a = s.define('A', ('1',))
        b = s.define('B', ('2', a, nt('E')))
        s.define('C', {'3', '4'})
        d = s.define('D', {('5', '6'), ('7', '8')})
        s.define('E', ('9', ('10', ('11', '12'), ('13', a) ) ))
        s.define('F', {(b, '14'), ('c', 'b'), (d, '15')}, target=True)

        LL1(s)

    #@unittest.SkipTest
    def test_empty_sketch(self):
        self.assertRaises(LLError, LL1, Sketch())

    #@unittest.SkipTest
    def test_implicit_target(self):
        s = Sketch()
        s.define('A', ('1', '2', '3'))

        LL1(s)

    #@unittest.SkipTest
    def test_non_productive_self_reference(self):
        s = Sketch()
        s.define('A', (nt('A'),), target=True)
        self.assertRaises(LLError, LL1, s)

    #@unittest.SkipTest
    def test_non_productive_self_reference_chain(self):
        s = Sketch()
        s.define('A', ['aaa', nt('A')], target=True)
        self.assertRaises(LLError, LL1, s)

    #@unittest.SkipTest
    def test_productive_self_reference_deep(self):
        import json

        s = Sketch()
        s.define('A', ['aaa'])
        s.define('S', {(nt('A'), 'bbb'),
                        ('bbb', nt('S'))}, target=True)

        ll1 = LL1(s)
        ll1.parse(tokens(['aaa', 'bbb']))
        ll1.parse(tokens(['bbb', 'bbb', 'bbb', 'aaa', 'bbb']))

    #@unittest.SkipTest
    def test_initials(self):
        s = Sketch()
        a = s.define('A', ['a'], or_lambda=True)
        b = s.define('B', {(a, '1'), (nt('C'), '2')})
        c = s.define('C', ('c', b, '3'))
        s.define('S', {(a, c), 's'}, target=True)

        ll1 = LL1(s)

        self.assertEqual(ll1['A'].initials.keys(), ['a',])
        self.assertEqual(set(ll1['B'].initials.keys()), {'a', 'c', '1'})
        self.assertEqual(ll1['C'].initials.keys(), ['c'])
        self.assertEqual(set(ll1['S'].initials.keys()), {'a', 'c', 's'})

    #@unittest.SkipTest
    def test_followers(self):
        s = Sketch()
        s.define('A', (nt('B'), nt('C')), target=True)
        s.define('B', ('b1', nt('X')))
        s.define('C', (nt('D'),))
        s.define('D', ('d',))
        s.define('X', ('x',))

        ll1 = LL1(s)
        self.assertEqual(ll1['A'].followers, {eof_token_name})
        self.assertEqual(ll1['B'].followers, {'d'})
        self.assertEqual(ll1['C'].followers, {eof_token_name})
        self.assertEqual(ll1['D'].followers, {eof_token_name})
        self.assertEqual(ll1['X'].followers, {'d'})

    #@unittest.SkipTest
    def test_followers_with_lambdas(self):
        s = Sketch()
        a = s.define('A', ('a',), or_lambda=True)
        b = s.define('B', ('b',), or_lambda=True)
        x = s.define('X', ('x',), or_lambda=True)
        s.define('S', (a, b, x), target=True)

        ll1 = LL1(s)
        self.assertEqual(ll1['A'].followers, {'b', 'x', eof_token_name})

    #@unittest.SkipTest
    def test_parse_lambda(self):
        s = Sketch()
        a = s.define('A', ['a'], or_lambda=True)
        b = s.define('B', ['b'])
        s.define('S', [a, a, a, a, a, b], target=True)

        ll1 = LL1(s)
        ll1.parse(tokens(['b']))
        self.assertRaises(LLError, ll1.parse, tokens(['a', 'c']))

    #@unittest.SkipTest
    def test_expected_eof(self):
        s = Sketch()
        a = s.define('A', ['a'], or_lambda=True)
        b = s.define('B', ['b'])
        s.define('S', [a, b], target=True)

        ll1 = LL1(s)
        self.assertRaises(LLError, ll1.parse, 'abc')
        self.assertRaises(LLError, ll1.parse, 'bc')

    #@unittest.SkipTest
    def test_arithmetic_lang(self):
        s = Sketch()

        s.define('S', [nt('E'), nt('F'), nt('R')], target=True)

        s.define('R', {
            ('+', nt('E'), nt('F'), nt('R')),
            ('-', nt('E'), nt('F'), nt('R'))}, or_lambda=True)

        s.define('F', {
            ('*', nt('E'), nt('F')),
            ('/', nt('E'), nt('F'))}, or_lambda=True)

        s.define('E', {
            ('(', nt('S'), ')'),
            'a',
            'b'})

        ll1 = LL1(s)
        tree = ll1.parse(tokens('a+a*b'))
        #print tree.as_json()

        self.assertRaises(LLError, ll1.parse, 'a+(a*b')

    #@unittest.SkipTest
    def test_eliminate_lr(self):
        s = Sketch()
        s.define('A', {(nt('A'), 'a0'), 'a1', 'a2'}, or_lambda=True, target=True)
        s.define('B', {(nt('A'), 'b0'), 'b1'})

        self.assertRaises(LLError, LL1, s)
        LL1(s, 'rf')

    #@unittest.SkipTest
    def test_lambda_calc(self):
        s = Sketch()
        # Do not specify or_lambda=True flag
        s.define('A', (nt('B'), nt('B')), target=True)
        s.define('B', ('b0', 'b1'), or_lambda=True)

        self.assertTrue('A' in LL1(s).lambda_syms)

    #@unittest.SkipTest
    def test_factoring_t(self):
        s = Sketch()

        # Note implicit chain A.0 :: ('b', 'c')
        s.define('A', {('a', ('b', 'c')), ('a', 'x')}, target=True)

        ll1 = LL1(s, 'f')
        self.assertEqual(len(ll1['A'].chains), 1)

    # @unittest.SkipTest
    def test_factoring_nt(self):
        s = Sketch()

        # Note implicit chain A.0 :: ('b', 'c')
        s.define('A', {(nt('B'), 'a1'), (nt('B'), 'a2')}, target=True)
        s.define('B', ('b1', 'b2'))

        LL1(s, 'f')

    # @unittest.SkipTest
    def test_recursion_and_factoring(self):
        s = Sketch()

        # Note implicit chain A.0 :: ('b', 'c')
        b = s.define('B', {(nt('A'), 'b1'),
                           (nt('A'), 'b2')})
        s.define('A', {(nt('A'), 'a1'),
                       ('a2',)}, target=True)

        self.assertRaises(LLError, LL1, s)
        self.assertRaises(LLError, LL1, s, 'r')
        self.assertRaises(LLError, LL1, s, 'f')

        LL1(s, 'rf')
        LL1(s, 'fr')

    #@unittest.SkipTest
    def test_brackets_language(self):
        s = Sketch()
        s.define('S', (nt('B'), nt('S')), or_lambda=True, target=True)
        s.define('B', {('(', nt('S'), ')'),
                       ('[', nt('S'), ']')}, or_lambda=True)

        text = '([()()])([[()()()]])'
        ll1 = LL1(s)
        ll1.parse(tokens(text))

        #print json.dumps(top, indent=2)


if __name__ == "__main__":
    unittest.main()
