#!/usr/bin/env python


def is_nt(obj):
    return isinstance(obj, NT)


class Def(object):
    def __init__(self, sym, chains, can_be_lambda=False):
        self.sym = sym
        self.chains = tuple(filter(None, chains))
        self.can_be_lambda = can_be_lambda or self.chains != tuple(chains)

        self.initials = dict()
        self.followers = set()

    def __repr__(self):
        return self.to_str(1)

    def to_str(self, width):
        chains = sorted(self.chains,
                        key=lambda c: tuple(x.sym if is_nt(x) else x for x in c))

        header_fmt = '{{:{:d}s}} : '.format(width)
        header = header_fmt.format(self.sym)

        def sym_to_str(sym):
            return str(sym)

        lines = []
        fmt_str = '{{:{:d}s}}{{}}'.format(width+3)
        for i, chain in enumerate(chains):
            lines.append(fmt_str.format(header if i == 0 else '',
                                        ' '.join(map(sym_to_str, chain))))

        if self.can_be_lambda:
            lines.append(fmt_str.format('', '.-'))

        return '\n'.join(lines)


class NT(object):
    """ Non-terminal symbol. """
    def __init__(self, sym):
        self.sym = sym

    def __repr__(self):
        return '.{}'.format(self.sym)

    def __eq__(self, other):
        return isinstance(other, NT) and self.sym == other.sym

    def __hash__(self):
        return hash(self.sym)


def nt(sym):
    return NT(sym)

