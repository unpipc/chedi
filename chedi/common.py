def one_string(objects):
    return ', '.join(map(str, objects))


def new_sym(sym, indicator='.', new_level=False):
    if indicator in sym:
        if new_level:
            head = sym
            n = 0
        else:
            bits = sym.split(indicator)
            head = indicator.join(bits[:-1])
            n = int(bits[-1]) + 1
    else:
        head = sym
        n = 0

    return '{}{}{:d}'.format(head, indicator, n)
