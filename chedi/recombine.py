#!/usr/bin/env python

import re

keyword = re.compile('^(?P<DEF>def)|(?P<IF>if)|(?P<STRING>"([^"\\\\]|(\\\\.))*")$')
#keyword = re.compile('def')

m = keyword.match('"A slash: <\\\\>\nA quote: <\\">"')

if m:
    print m.span()
    key_type, key_value = [(k, v) for k, v in m.groupdict().items() if v is not None][0]
    print "{}: {}".format(key_type, key_value)
else:
    print "No match."