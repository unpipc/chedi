class LLError(Exception):
    pass


def claim(cond, err_msg):
    if not cond:
        raise LLError(err_msg)


def syntax_error(cursor, is_eof):
    if is_eof:
        err_msg = 'Unexpected end of sequence at {:d}'.format(cursor)
    else:
        err_msg = 'Syntax error at {:d}'.format(cursor)

    raise LLError(err_msg)


def expected_eof(cursor):
    raise LLError('Expected end of file at {:d}'.format(cursor))

